## Leafony for Beta65S ハードウェア

### ブロック図

```mermaid
graph TB

  SubGraph1 --> BaseBoard
  subgraph "Base   board"
  BaseBoard(Leafony bus)
  BaseBoard -- I2C --> MPU-6050
  BaseBoard -- I2C --> INA226
  BaseBoard -- PWM*4 --> STSPIN240
  end

  subgraph "ESP32 MCU Leaf"
  Node1[ESP32-WROOM-32] --> SubGraph1[Leafony bus]
end
```

### マイクロドローン機体

* [マイクロドローン・フレーム (ホイールベース 65㎜)](https://betafpv.com/collections/brushed-frames/products/65mm-micro-whoop-frame-for-7x16mm-motors-v4-version)
* [ブラシ付きモーター (7x16mm)](https://betafpv.com/collections/brushed-motors/products/7x16mm-19000kv-brushed-motors-2cw-2ccw)
* [プロペラ (31mm)](https://betafpv.com/collections/31mm-propellers/products/31mm-3-blade-micro-whoop-propellers-1-0mm-shaft-1)
* [電池 (LiPo 1S)](https://betafpv.com/collections/batt-1s/products/300mah-1s-30c-hv-battery-8pcs)
* 電池用コネクタ　(JST-PH2.0)

### 使用したLeafony機材

* [AP02 ESP32 MCU](https://docs.leafony.com/docs/leaf/processor/ap02/)
* [AX04 Spacer](https://docs.leafony.com/docs/leaf/extension/ax04/)
* [AZ61 Connector](https://docs.leafony.com/docs/leaf/others/az61/)
  
### 自作基板

* 加速度・ジャイロセンサー：MP6050
* モーター制御IC：STSPIN240
* 電源IC：TPS73633
* 電源監視IC：INA226
* 大きさ：26.5 x 26.5 mm の取付穴に合わせた(Whoopでの標準)
* ツール：KiCADで設計
  * [Leafony bus フットプリント](https://docs.leafony.com/docs/pcb/kicad/) を利用

自作基板の 回路図は[こちらへ](./images/Leafony_for_Beta65S_回路図.pdf)

自作基板の 外形図・部品配置図
![外形図・部品配置図](./images/Leaf-B65S_PCB.png)

### 評価進捗状況

* [x] STSPIN240のモータ側の抵抗は０とした（電流保護未使用）
* [ ] INA226の電圧・電流の測定精度は未検証
* [x] D10信号ラインに挿入したインバーターは外した方が良い。STM32のタイマーチャンネルの一つを利用する意図であったが、電源投入時にモーターが回転して危険である。

#### 本資料について

本資料は[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/deed.ja)の元で公開します。

![クリエイティブ・コモンズ・ライセンス](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)　2021c計画工学研究所
