# Leafony for Beta65S Software

Beta65Sの制御基板（Flight Controller）はBetaFlightというオープンソースのプログラムがサポートされていますが、本作品ではArduinoで動作するESPCopterを選びました。

## ESPCopter

ESPcopterはESP8266マイコンを使いブラシ付きモーターを駆動するマイクロドローンです。

* 拡張コネクタを有しオプション製品も多くあります。
* プログラム環境を複数用意し、STEM教育機材として利用できます。
* [ESPCopter のコード](https://espcopter.com/code-release/)
  * Arduino向けソースコード、Version 1.0.3 Beta - 27.05.2020
  * このサイトはトルコにある様子で、通信環境ないしサーバーメインテナンスの都合か時々アクセスできない事があります。

## ESPcopterとLeaf-B65Sの比較写真

  ![比較写真](./images/ESPCopter_Leaf-B65S.JPG)

## 移植作業

ESPcopterのマイコンはESP8266であるが、ESP32マイコンLeafを利用します。これらは同じ会社のマイコンとは言え世代が異なりAPIの一部が異なるためソースコードの変更をしました。

* WiFi関連のAPIが異なる
* ESP8266ではPWM関数を利用しているが、ESP32ではLEDC関数を使う

ハードウェアの違いもあります。

* ESPCopterではコンパス（Mag）センサーがあるが、自作基板には無いので、Mag周りのソースコードをコメントアウトした
* 6軸センサーのI2Cアドレスが異なる
* 自作基板では電圧電流評価用のINA226を追加したので、I2C経由のポートアクセスを追加した

ソースコードの変更箇所は ”//K3” の文字列で検索して下さい。

## 制御ソフトウェア

* [RemoteXY](https://remotexy.com/)というスマホ・アプリで飛行制御しました。

### 評価進捗状況

* [x] RemoteXYで実験した
* [ ] RemoteXY以外の制御方法は未評価
* [ ] オプション製品が幾つがあるが、未評価

#### 本資料について

本資料は[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/deed.ja)の元で公開します。

![クリエイティブ・コモンズ・ライセンス](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)　2021c計画工学研究所
