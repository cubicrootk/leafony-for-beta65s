
// Include of Arduino

#include "Arduino.h"

void scanShields(){
  Wire.begin();
  byte error, address;
  int nDevices;
 
  Serial.println("Scanning...");
 delay(1500);
  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      Serial.print("I2C device found at address "); //K3 in deciaml, not hexa
      if (address<16)
      Serial.print("0");
      Serial.print(address);
      Serial.println("  !");

      //K3 begin
      if(address == 104 || address == 105){
      Serial.print("MPU is found at "); Serial.println(address);
      Wire.beginTransmission(address);
      Wire.write(PWR_MGMT_1); // PWR_MGMT_1 register 0x6B
      Wire.write(0);     // zero to wakes up the MPU-6050
      Wire.endTransmission(true);
      Wire.beginTransmission(address);
      Wire.write(WHO_AM_I);
      Wire.endTransmission(true);
      Wire.requestFrom(address, (byte) 1); //K3 compiler warning for the second parameter /*length*/ 
      Serial.print("  WHO_AM_I is ");
      Serial.print(byte (Wire.read()));
      Serial.println("; 104 for MPU6050, 112 for MPU6500, 113 for MPU9250.");
      
      Wire.beginTransmission(address);
      Wire.write(ACCEL_XOUT_H);  // 0x3B
      Wire.endTransmission(true);
      Wire.requestFrom(address, (byte) 14); //K3 compiler warning for the second parameter
      while (Wire.available() < 14); // wait to get 14 bytes ready? 
      int16_t axRaw, ayRaw, azRaw, gxRaw, gyRaw, gzRaw, mpTemp;
      axRaw = Wire.read() << 8 | Wire.read();
      ayRaw = Wire.read() << 8 | Wire.read();
      azRaw = Wire.read() << 8 | Wire.read();
      mpTemp = Wire.read() << 8 | Wire.read();
      gxRaw = Wire.read() << 8 | Wire.read();
      gyRaw = Wire.read() << 8 | Wire.read();
      gzRaw = Wire.read() << 8 | Wire.read();
      float acc_x = axRaw / 16384.0; //FS_SEL_0 16,384 LSB / g
      float acc_y = ayRaw / 16384.0; // Accelaration =
      float acc_z = azRaw / 16384.0; //   acc-raw/ resolution
      float gyro_x = gxRaw / 131.0;  //FS_SEL_0 131 LSB / (degree/s)
      float gyro_y = gyRaw / 131.0;  // Angular velocity =
      float gyro_z = gzRaw / 131.0;  //   gyro-raw/ resolution (131)
      Serial.print(acc_x);  Serial.print(", ");
      Serial.print(acc_y);  Serial.print(", ");
      Serial.print(acc_z);  Serial.print(",");
      Serial.print(" Gyro_x,y,z: ");
      Serial.print(gyro_x); Serial.print(", ");
      Serial.print(gyro_y); Serial.print(", ");
      Serial.print(gyro_z); Serial.println("");
      Serial.print("  Temperature "); Serial.print(float((mpTemp + 521)/ 340 + 35));
      Serial.println(". End of MPU-6xxx read out.");
      }      
      //K3 end
      if(address == 22 || address == 41){
      vl5310xControl = 1;
       Serial.println("  Altidude hold shield was found" + vl5310xControl);
        Serial.println(vl5310xControl);
      }
  
      if(address == 112 ){
      multiRangerControl = 1;
       Serial.println("  multi-ranger hold shield was found " + multiRangerControl);
      Serial.println(multiRangerControl);
      } 

      if(address == 119 ){
      bme280Control = 1;
      Serial.println("  BME280 shield was found " + bme280Control);
      Serial.println(bme280Control);
      } 

      if(address == 8 ){
      opticalFlowControl = 1;
       Serial.println("  Optical hold shield was found " + opticalFlowControl);
      Serial.println(opticalFlowControl);
      }
      
      if(address == 9 ){
      lpsControl = 1;
       Serial.println("  dwml000 hold shield was found " + lpsControl);
       Serial.println(lpsControl);
      }

      
      nDevices++;
      }
     else if (error==4)
    {
      Serial.print("Unknown error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");

}


void mainSetup(){

  // Set Pin Mode
  Serial.begin(115200);// 921600   

  while (! Serial) {
    delay(1);
  }

//K3 begin
#ifdef ESP8266  
  //analogWriteFreq(20000); 
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D6, OUTPUT);
  pinMode(D7, OUTPUT);
  pinMode(D8, OUTPUT);

  digitalWrite(D1, LOW);
  digitalWrite(D2, LOW);
  digitalWrite(D3, LOW);
  digitalWrite(D4, LOW);
  digitalWrite(D5, LOW);
  digitalWrite(D6, LOW);
  digitalWrite(D7, LOW);
  digitalWrite(D8, LOW);

  ////// Initialize the PWM

  // Initial duty -> all off

  for (uint8_t channel = 0; channel < PWM_CHANNELS; channel++) {
    pwm_duty_init[channel] = 0;
  }

  // Period

  uint32_t period = PWM_PERIOD;

  // Initialize

  pwm_init(period, pwm_duty_init, PWM_CHANNELS, io_info);

  // Commit

  pwm_start();
#endif
#ifdef ESP32
// GPIO definition for NodeMCU(ESP8266) ; D0 = GPIO16(Green LED)
// D1 = GPIO5(SCL); D2 = GPIO4(SCA); D3 = GPIO00(Blue); D4 = GPIO02(Red);
// D5 = GPIO14(FR); D6 = GPIO12(RL); D7 = GPIO13(RR); D8 = GPIO15(FL);
// GPIO definiio for Leafony ESP32-DevKit(WROOM-32); 
// D0 = IO03(RXD0); D1 = IO01(TXD0); D2 = GPIO04; D3 = GPIO27; 
// D4 = IO12(RL); D5 = IO13(RR); D6 = IO14(FR); D7 = IO15(FL); 
// D11= IO23(blue); D12= IO19(Red); D13= IO18(green), SDA= IO21, SDL= IO22
// To Do (Buzzer ?)

// Setting PWM properties
// Initial duty -> all off
// void pwm_set_duty(uint32_t duty, uint8_t channel)
// channel definition is actually in FlightControl.h
//  pwm_set_duty(0, 0); // channel 0 (FL=M1) = gpio12 (M0=gpio 15)
//  pwm_set_duty(0, 1); // channel 1 (RL=M4) = gpio14 (M3=gpio 12)
//  pwm_set_duty(0, 2); // channel 2 (RR=M3) = gpio13 (M2=gpio 13)
//  pwm_set_duty(0, 3); // channel 3 (FR=M2) = gpio27 (M1=gpio 14)
  const int motorPin1 = 12; // PWM pin for Motor 1(FL), Leaf F26=D4
  const int motorPin2 = 27; // PWM pin for Motor 2(FR), Leaf F24=D3 //D6=GIO27 <-> D10=GIO05
  const int motorPin3 = 13; // PWM pin for Motor 3(RR), Leaf F28=D5
  const int motorPin4 = 14; // PWM pin for Motor 4(RL), Leaf F07=D6 
  const int pwmCh1 = 1;
  const int pwmCh2 = 2;
  const int pwmCh3 = 3;
  const int pwmCh4 = 4;
  const int freq = 1000; // tha same as ESPcopter and Arduino higher pwm freq
  const int resolution = 8; // PMW dutyCycle in 8bit. 0 for off, 255 for On
  int dutyCycle = 0;
  ledcSetup(pwmCh1, freq, resolution);
  ledcAttachPin(motorPin1, pwmCh1); // PWM pin for Motor 1(FL), Leaf F26=D4=gpio12 (ex. M0=gpio 15)
  ledcWrite(pwmCh1, dutyCycle);   
  ledcSetup(pwmCh2, freq, resolution);
  ledcAttachPin(motorPin2, pwmCh4); // PWM pin for Motor 2(FR), Leaf F24=D3=gpio27 (ex. M1=gpio 14)
  ledcWrite(pwmCh2, dutyCycle);   
  ledcSetup(pwmCh3, freq, resolution);
  ledcAttachPin(motorPin3, pwmCh3); // PWM pin for Motor 3(RR), Leaf F28=D5=gpio13 (ex. M2=gpio 13)
  ledcWrite(pwmCh3, dutyCycle);  
  ledcSetup(pwmCh4, freq, resolution);
  ledcAttachPin(motorPin4, pwmCh2); // PWM pin for Motor 4(RL), Leaf F07=D6=gpio14 (ex.M3=gpio 12)
  ledcWrite(pwmCh4, dutyCycle);  

  pinMode(23, OUTPUT); // Leaf-D11 MOSI
  pinMode(19, OUTPUT); // Leaf-D12 MISO
  pinMode(18, OUTPUT); // Leaf-D13 SLK of VSPI
  digitalWrite(23, LOW);
  digitalWrite(19, LOW);
  digitalWrite(18, LOW);
  Serial.println("mainSetup in progress");
#endif 
//K3 end

  esp.redLed_Digital(false);
  esp.blueLed_Digital(false);
  esp.greenLed_Digital(false);
  
  
  EEPROM.begin(512);
  scanShields();

//K3 begin
// INA226 setup
  Serial.println("Starting INA226");
  Wire.begin(); // leaf SCL=F15 is IO22, SDA=F23 is IO21 as default
  if (!INA.begin() ) {Serial.println(" could not connect. Fix and Reboot");  }
  INA.setMaxCurrentShunt(0.3, 0.01); //(float maxCurrent, float shunt, bool normalize = True)
  INA.setAlertRegister(0); // no alert set
  INA.setAverage(4); // 4 for 128, default convertion time is 1.1ms
  Serial.println("  BUS\tSHUNT\tCURRENT\tPOWER");
  Serial.print("  ");
  Serial.print(INA.getBusVoltage(), 3);
  Serial.print("\t");
  Serial.print(INA.getShuntVoltage_mV(), 3);
  Serial.print("\t");
  Serial.print(INA.getCurrent_mA(), 3);
  Serial.print("\t");
  Serial.print(INA.getPower_mW(), 3);
  Serial.println();
// end of INA226 setup  

//K3 end

  ahrs.Initialize();

  if(Trim_DF == 1){
  EEPROM.write(60,highByte(Trim_Roll_DF));
  EEPROM.write(61,lowByte(Trim_Roll_DF));

  EEPROM.write(62,highByte(Trim_Pitch_DF));
  EEPROM.write(63,lowByte(Trim_Pitch_DF));

  EEPROM.write(64,highByte(Trim_Yaw_DF));
  EEPROM.write(65,lowByte(Trim_Yaw_DF));
  
  EEPROM.commit();
  }

  Trim_Roll_Bs = word(EEPROM.read(60),EEPROM.read(61));
  Trim_Pitch_Bs = word(EEPROM.read(62),EEPROM.read(63));
  Trim_Yaw_Bs = word(EEPROM.read(64),EEPROM.read(65));

  yaw.SetItermRate(Trim_Yaw_Bs);

  
   roll.SetGain(15.2, 25.0, 0.18,0.0,1.4);//     roll.SetGain(15.2, 25.0, 0.18,0.0,1.4);
   roll.SetLimit(4500, 3000, 6000);  //  roll.SetLimit(4500, 3000, 6000); 
   
   pitch.SetGain(15.2, 25.0, 0.18,0.0,1.4); //    pitch.SetGain(15.2, 25.0, 0.18,0.0,1.4);
   pitch.SetLimit(4500, 3000, 6000);   //    pitch.SetLimit(4500, 3000, 6000); 
   
   yaw.SetGain(0.8,0.3,0.5,0.9,0.0); // (0.8,0.3,0.5,0.9,0.0);
   yaw.SetLimit(4500, 1000, 6000); 
   
   oto.SetGain(0.0, 0.0, 1.8,1.5,750.0);  // 1.8,3.5,1000.0); 
   oto.SetLimit(1000, 1000, 6500); 
   
   yOpt.SetGain(0.0, 0.0, 0.012, 0.05 ,0.0); //0.012(++) ,0.05 ,0.0(~));
   yOpt.SetLimit(6500, 1000, 10000); //4500, 1000, 10000
    
   xOpt.SetGain(0.0, 0.0, 0.012, 0.05 ,0.0);//0.02
   xOpt.SetLimit(6500, 1000, 10000); //3000, 900, 3500

   xMulti.SetGain(0.0, 0.0, 1.6 , 0.4 ,125.0); //  0.9 ,0.4 ,125); // 1.6 ,0.6 ,125.0);
   xMulti.SetLimit(4500, 1000, 6000); //3000, 900, 3500
    
   yMulti.SetGain(0.0, 0.0, 1.6 , 0.4 ,125.0);
   yMulti.SetLimit(4500, 1000, 6000); //3000, 900, 3500

   xMultiM.SetGain(0.0, 0.0, 1.6 , 0.4 ,125.0); //  0.9 ,0.4 ,125); // 1.6 ,0.6 ,125.0);
   xMultiM.SetLimit(4500, 1000, 6000); //3000, 900, 3500
    
   yMultiM.SetGain(0.0, 0.0, 1.6 , 0.4 ,125.0);
   yMultiM.SetLimit(4500, 1000, 6000); //3000, 900, 3500
   

   #ifdef bme280
   if(bme280Control == 1){
   bmeSetup();
   }
   #endif
     
   #ifdef vl53l0x
   if(vl5310xControl == 1){
   InitVL53L0X();
   }
   #endif

   #ifdef opticalFlow
   if(opticalFlowControl){
   opticalSensorSetup();
   }
   #endif
      
   #ifdef MULTI_RANGER
   if(multiRangerControl ==1){
   multiRangerSetup();
   }
   #endif
   
    
  esp.redLed_Digital(false);
  esp.blueLed_Digital(false);
  esp.greenLed_Digital(true);


  setupWiFi();
   
}
