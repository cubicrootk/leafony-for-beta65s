#include "arduino.h"

//K3 begin
#ifdef ESP32
#include <analogWrite.h>  //erropix/ESP32 AnalogWrite@^0.2
#endif
//K3 end

class ESPCOPTER{
  
  public:
  void espcopterSetup();
  void redLed_Digital(int OnOff);
  void blueLed_Digital(int OnOff);
  void greenLed_Digital(int OnOff);

  void redLed_Digital(boolean OnOff);
  void blueLed_Digital(boolean OnOff);
  void greenLed_Digital(boolean OnOff);
/*
  void redLed_Analog(int value);
  void blueLed_Analog(int value);
  void greenLed_Analog(int value);
*/
  void buzzer(boolean OnOff, int timer);

  void motorFL_Analog(int value);
  void motorFR_Analog(int value);
  void motorRL_Analog(int value);
  void motorRR_Analog(int value);
  
//K3 begin
#ifdef ESP8266  
  #define blueLed_  0
  #define redLed_   2
  #define greenLed_ 16
#endif
#ifdef ESP32
// GPIO definition for NodeMCU(ESP8266) ; D0 = GPIO16(Green LED)
// D1 = GPIO5(SCL); D2 = GPIO4(SCA); D3 = GPIO00(Blue); D4 = GPIO02(Red);
// D5 = GPIO14(FR); D6 = GPIO12(RL); D7 = GPIO13(RR); D8 = GPIO15(FL);
// GPIO definiio for Leafony ESP32-DevKit(WROOM-32); 
// D0 = IO03(RXD0); D1 = IO01(TXD0); D2 = GPIO04; D3 = GPIO27; 
// D4 = IO12(RL); D5 = IO13(RR); D6 = IO14(FR); D7 = IO15(FL); 
// D11= IO23(blue); D12= IO19(Red); D13= IO18(green), SDA= IO21, SDL= IO22
  #define blueLed_  23
  #define redLed_   12
  #define greenLed_ 18
#endif
//K3 end
  private:
  unsigned long previousMillisBuzzer = 0;
  boolean buzzerState = LOW;
  
  };
