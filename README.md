# マイクロドローン ～ Leaf-B65S製作 ～

トリリオンノード研究会が開発し LEAFONY SYSTEMS 株式会社が販売する[Leafonyシステム](https://docs.leafony.com/docs/overview/)を応用した[製作記事です](https://trillion-node.org/forums/topic/20220217/)。

## 製作内容と目的

ドローンのジャンルにFPV (First Person View)があり、ホビーやレース目的で人気なのがマイクロドローン(Tiny Whoopとも呼ばれる)です。
この製作記事はBeta65Sという市販品の基板をLeafonyシステムで換装したものです。
三次元飛行制御の理解に留まらず、CO2センサー等を追加搭載して環境センシングや、AIエンジン追加で衝突回避等、今後も応用先が広がると期待してます。

## ハードウェア

* BetaFPV社が販売するBeta65Sマイクロドローンのフレーム、モーター、プロペラ部品等を購入
* その制御基板と同じ大きさの自作PCBを作成し、ESPマイコンを搭載したESP32 Leafと組合わせた

* ハードウェアの詳細は[こちらへ](./hardware/Leafony-B65S_Hardware.md)

## ソフトウェア

* [ESPCopter](https://espcopter.com/) を”自作基板＋ESP32 Leaf” に移植しました

* ソフトウェアの詳細については[こちらへ](./src/Leafony-B65S_Software.md)

## ビデオ

* [説明ビデオ](https://youtu.be/dY5N4LQ9faU)
  [![youtube](./images/Leaf-B65S_photo1.png)](https://youtu.be/dY5N4LQ9faU)
* [RemoteXY](https://remotexy.com/)というスマホ・アプリで飛行制御しました

### 本資料について

本資料は[CC BY-SA 4.0（クリエイティブ・コモンズ 表示 4.0 継承 国際 ライセンス）](http://creativecommons.org/licenses/by-sa/4.0/deed.ja)の元で公開します。

![クリエイティブ・コモンズ・ライセンス](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)　2022c計画工学研究所
